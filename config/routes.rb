Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions', omniauth_callbacks: 'users/omniauth_callbacks' }
  devise_scope :user do
    get 'users/profile' => 'users/registrations#profile'
  end

  resources :users, :only => [:index, :show]
  resources :users do
    member do
      get :following, :followers
    end
  end

  resources :microposts, :only => [:new, :create, :destroy, :show]
  resources :relationships, :only => [:create, :destroy]
  root 'static_pages#home'
end
