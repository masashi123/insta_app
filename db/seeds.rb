User.create!(username:  "Example User",
             fullname: "Example",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar")

50.times do |n|
  username  = Faker::Name.name
  fullname  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(username:  username,
               fullname: fullname,
               email: email,
               password:              password,
               password_confirmation: password)
end

# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
