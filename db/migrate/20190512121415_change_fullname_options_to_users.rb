class ChangeFullnameOptionsToUsers < ActiveRecord::Migration[5.2]
  def up
    change_column :users, :fullname, :string, null: false, default: ""
  end

  def down
    change_column :users, :fullname, :string
  end
end
