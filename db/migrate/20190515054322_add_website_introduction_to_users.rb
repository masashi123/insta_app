class AddWebsiteIntroductionToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :website, :string
    add_column :users, :introduction, :text
  end
end
