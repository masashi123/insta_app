class ChangeUserNameOptionsToUsers < ActiveRecord::Migration[5.2]
  def up
    change_column :users, :username, :string, null: false, default: ""
  end

  def down
    change_column :users, :username, :string
  end
end
