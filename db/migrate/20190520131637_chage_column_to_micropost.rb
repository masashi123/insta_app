class ChageColumnToMicropost < ActiveRecord::Migration[5.2]
  def up
    change_column :microposts, :picture, :string, null: false
  end

  def down
    change_column :microposts, :picture, :string
  end
end
