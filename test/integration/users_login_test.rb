require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @user = users(:john)
  end

  test "login with invalid information" do
    get new_user_session_url
    assert_template 'devise/sessions/new'
    post user_session_path, params: { users: { email: "", password: "" } }
    assert_template 'devise/sessions/new'
  end

  test "login with valid information" do
    get new_user_session_url
    assert_template 'devise/sessions/new'
    post user_session_path, params: { email: @user.email, password: "password" }
    assert_response :success
  end

  test "login with valid information followed by logout" do
    get new_user_session_url
    assert_template 'devise/sessions/new'
    post user_session_path, params: { email: @user.email, password: "password" }
    assert_response :success
    delete destroy_user_session_url
    assert_redirected_to root_url
  end
end
