require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "invalid signup information" do
    get new_user_registration_path
    assert_no_difference 'User.count' do
      post user_registration_path, params: { fullname: "",
                                         usernam: "",
                                         email: "",
                                         password:              "foo",
                                         password_confirmation: "bar" }
    end
    assert_template 'devise/registrations/new'
    assert_select 'div#error_explanation'
  end

  test "valid signup information" do
    get new_user_registration_path
    assert_difference 'User.count', 1 do
    post user_registration_path, params: { user: { fullname:  "Example User",
                                         username: "Example",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }

    end
    follow_redirect!
    assert_template 'devise/registrations/profile'
  end
end
