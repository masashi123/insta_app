require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @user = users(:john)
  end

  test "unsuccessful edit" do
    get new_user_session_url
    assert_template 'devise/sessions/new'
    post user_session_path, params: { email: @user.email, password: "password" }
    get edit_user_registration_url(@user)
    patch user_registration_url(@user), params: { username:  "",
                                             email: "foo@invalid",
                                             password:              "foo",
                                             password_confirmation: "bar" }
    assert_response 401
  end

  test "successful edit" do
    sign_in(@user)
    get edit_user_registration_url(@user)
    patch user_registration_path, params: { username:  "a",
                                             email: "foo@valid.com",
                                             password:              "foo",
                                             password_confirmation: "foo"                      }
    assert_response :success
  end
end
