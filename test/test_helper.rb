ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  #新規登録
  def sign_in(user)
    get new_user_session_url
    assert_template 'devise/sessions/new'
    post user_session_path, params: { email: user.email, password: user.password }
  end
end
