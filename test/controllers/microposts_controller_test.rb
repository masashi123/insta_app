require 'test_helper'

class MicropostsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @micropost = microposts(:orange)
    @user = users(:john)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: "Lorem ipsum" } }
    end
    assert_response 302
  end

  test "should redirect destroy when not logged in" do
   assert_no_difference 'Micropost.count' do
     delete micropost_path(@micropost)
   end
   assert_response 302
  end

  test "should redirect destroy for wrong micropost" do
   sign_in(users(:michael))
   micropost = microposts(:orange)
   assert_no_difference 'Micropost.count' do
     delete micropost_path(micropost)
   end
   assert_response 302
  end
end
