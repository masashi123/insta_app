require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @user = users(:john)
    @otheruser = users(:michael)
  end

  test "should get show" do
    login_as(@user, scope: :user)
    get user_url(@user)
    assert_response :success
  end

  test "should get index" do
    login_as(@user, scope: :user)
    get users_url
    assert_response :success
  end

  test "should redirect index when not logged in" do
    get users_path
    assert_response 302
  end

end
