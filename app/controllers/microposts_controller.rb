class MicropostsController < ApplicationController
  before_action :authenticate_user!, only:[:create, :destroy]

  def new
    @micropost = Micropost.new
  end

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "作成されました!"
      redirect_to users_profile_url
    else
      render 'static_pages/home'
    end
  end

  def show
    @micropost = Micropost.find_by(id: params[:id])
  end

  def destroy
    @micropost = current_user.microposts.find_by(id: params[:id])
    @micropost.destroy
    flash[:success] = "削除されました！"
    redirect_to request.referrer || root_url
  end

  private

  def micropost_params
    params.require(:micropost).permit(:picture, :content)
  end
end
