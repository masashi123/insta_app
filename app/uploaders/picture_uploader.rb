class PictureUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  process resize_to_fit: [800, 800]

  storage :file

  #保存先
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  #アップロード可能な拡張子
  def extension_whitelist
    %w(jpg  png)
  end

  version :thumb do
    process resize_to_fill: [200, 200]
  end
end
